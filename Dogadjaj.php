<?php

class Dogadjaj{
    public $id;
    public $naziv;
    public $datum_pocetka;
    public $cena;
    public $duzina_trajanja;
    public $vreme_odrzavanja;
    public $aktivan;
    public $opis;
    public $url_slike;
	public $idIzabraneSale;
	public $prosecnaOcena;

    function __construct($id,$naz,$datum,$cena,$duzina,$aktivan,$opis,$url,$idIzabraneSale,$prosecnaOcena,$vreme_odrzavanja) {
        $this->id=$id;
        $this->naziv=$naz;
        $this->datum_pocetka=$datum;
        $this->duzina_trajanja=$duzina;
        $this->cena=$cena;
        $this->aktivan=$aktivan;
        $this->opis=$opis;
        $this->url_slike=$url;
		$this->idIzabraneSale = $idIzabraneSale;
        $this->prosecnaOcena = $prosecnaOcena;
        $this->vreme_odrzavanja = $vreme_odrzavanja;
    }
}




?>