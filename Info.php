<?php

class Info{
    public $id;
    public $naziv;
    public $grad;
    public $ulica;
    public $radnoVreme;
    public $lokacijaX;
    public $lokacijaY;
    public $kontakt;


    function __construct($id,$naz,$grad,$ulica,$kontakt,$radnoVreme,$lokacijaY,$lokacijaX) {
        $this->id=$id;
        $this->naziv=$naz;
        $this->grad=$grad;
        $this->radnoVreme=$radnoVreme;
        $this->ulica=$ulica;
        $this->lokacijaY=$lokacijaY;
        $this->lokacijaX=$lokacijaX;
        $this->kontakt=$kontakt;

    }
}
?>