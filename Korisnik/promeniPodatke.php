<?php

include_once '../lib.php';
include_once '../Korisnik.php';

if (isset($_POST["promena"])) {
    $baza = new Baza();
    session_start();

    if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
        if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {
            $email = $_SESSION["EMAIL"];
            $korisnik = $baza->vratiKorisnika($_SESSION["EMAIL"]);
            $emailOdgovor = "";
            $imeOdgovor = "";
            $lozinkaOdgovor = "";
            $menjaj = false;
            if (isset($_POST["NovaLozinka"]) && isset($_POST["StaraLozinka"])) {
                if (password_verify($_POST["StaraLozinka"], $korisnik->hash)) {
                    $korisnik->hash = password_hash($_POST["NovaLozinka"], PASSWORD_DEFAULT);
                    $lozinkaOdgovor = (object) ["LozinkaGreska" => false, "Poruka" => "Lozinka promenjena"];

                    $menjaj = true;
                } else {
                    $lozinkaOdgovor = (object) ["LozinkaGreska" => true, "Poruka" => "Stara lozinka se ne poklapa"];
                }
            }
            if (isset($_POST["Email"])) {
                if ($baza->postojiKorisnik($_POST["Email"]) == false) {
                    $korisnik->email = $_POST["Email"];
                    $emailOdgovor = (object) ["EmailGreska" => false, "Poruka" => "Email uspesno promenjen"];
                    $_SESSION["EMAIL"] = $korisnik->email;
                    $menjaj = true;
                } else {
                    $emailOdgovor = (object) ["EmailGreska" => true, "Poruka" => "Vec postoji korisnik sa tim email-om"];
                }
            }
            if (isset($_POST["KorisnickoIme"])) {
                $menjaj = true;
                $korisnik->username = $_POST["KorisnickoIme"];
                $imeOdgovor = (object) ["ImeGreska" => false, "Poruka" => "Korisničko ime uspešno promenjeno"];
            }

            if (isset($_POST["Obavestenje"])) {
                $obavesti = $_POST["Obavestenje"];
                if ($obavesti != $korisnik->primajObavestenja) {
                    $korisnik->primajObavestenja = $obavesti;
                    $menjaj = true;
                }
            }
            if ($menjaj == true) {
                $baza->promeniKorisnikovePodatke($korisnik);
            }
            echo json_encode((object) ["Greska" => "NEMA", "Email" => $emailOdgovor, "Ime" => $imeOdgovor, "Lozinka" => $lozinkaOdgovor]);
            $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
        } else {
            session_destroy();
            echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
        }
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
    }
}
?>

