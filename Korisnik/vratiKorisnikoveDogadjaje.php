<?php
include_once '../lib.php';
include_once '../Korisnik.php';

$baza=new Baza();
if(isset($_POST["stranica"])){
    session_start();
    if(isset($_SESSION['PRIJAVLJEN'])&&$_SESSION['PRIJAVLJEN']==true){
        if(isset($_SESSION['POSLEDNJA_AKTIVNOST'])&&time()-$_SESSION['POSLEDNJA_AKTIVNOST']<3600)
        {
           
            $stranica=$_POST["stranica"];
            $brojDogadjaja=$_POST["brojDogadjaja"];
            if(isset($_POST["Id"])){
                $idKorisnika=$_POST["Id"];
            }
            else{
            $idKorisnika=$_SESSION["ID"];
            }
            $dogadjaji=$baza->vratiDogadjajeZaKorisnika(($stranica-1)*$brojDogadjaja,$brojDogadjaja,$idKorisnika);
            
            $ukupno=0;
            if(isset($_POST["vratiMax"])){
                $ukupno=$baza->vratiBrojDogadjajaZaKorisnika($idKorisnika);
            }
            
            echo json_encode((object)["Greska"=>"NEMA","Dogadjaji"=>$dogadjaji,"Ukupno"=>$ukupno]);

            $_SESSION['POSLEDNJA_AKTIVNOST']=time();
        }
        else{
            session_destroy();
            echo json_encode((object)["Greska"=>"SESIJA_ISTEKLA"]);
            
        }
    }
    else{
        session_destroy();
        echo json_encode((object)["Greska"=>"NIJE_PRIJAVLJEN"]);
    }
  }
?>

