<?php
include_once '../lib.php';
include_once '../Korisnik.php';

$baza=new Baza();

    session_start();
    if(isset($_SESSION['PRIJAVLJEN'])&&$_SESSION['PRIJAVLJEN']==true){
        if(isset($_SESSION['POSLEDNJA_AKTIVNOST'])&&time()-$_SESSION['POSLEDNJA_AKTIVNOST']<3600)
        {
            $email=$_SESSION["EMAIL"];
            
            $korisnik=$baza->vratiKorisnika($email);
            if($korisnik!==null){
            $korisnik->hash=null;
            $_SESSION['POSLEDNJA_AKTIVNOST']=time();
            echo json_encode((object)["Greska"=>"NEMA","Korisnik"=>$korisnik]);
            } else
            {
                echo json_encode((object)["Greska"=>"Ne postoji korisnik sa tim emailom"]);
            }
        }
        else{
            session_destroy();
            echo json_encode((object)["Greska"=>"SESIJA_ISTEKLA"]);
            
        }
    }
    else{
        session_destroy();
        echo json_encode((object)["Greska"=>"NIJE_PRIJAVLJEN"]);
    }
  
?>

