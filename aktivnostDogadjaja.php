<?php

include_once 'lib.php';
include_once 'Korisnik.php';

if (isset($_POST["idDogadjaja"]))
    ;
session_start();
if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
    if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {
        IF (isset($_SESSION['ADMIN']) && $_SESSION['ADMIN'] == 'DA') {
            $baza = new Baza();
            $idDogadjaja = $_POST["idDogadjaja"];
            $aktivnost = $_POST["Aktivan"];
            if ($baza->podesiAktivnost($idDogadjaja, $aktivnost))
                echo json_encode((object) ["Greska" => "NEMA"]);
        } else {
            echo json_encode((object) ["Greska" => "NISTE_ADMIN"]);
        }
        $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
    }
} else {
    session_destroy();
    echo json_encode((object) ["Greska" => "NISTE_PRIJAVLJENI"]);
}
?>

