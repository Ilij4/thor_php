<?php

include_once 'lib.php';

$baza = new Baza();
session_start();
if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
    if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {
        IF (isset($_SESSION['ADMIN']) && $_SESSION['ADMIN'] == 'DA') {
            $baza->azurirajInfo($_POST["naziv"], $_POST["grad"], $_POST["ulica"], $_POST["kontakt"], $_POST["radnoVreme"], $_POST["lokacijaX"], $_POST["lokacijaY"]);
            echo json_encode((object)["Greska"=>"NEMA"]);
        } else {
            echo json_encode((object) ["Greska" => "NISTE_ADMIN"]);
        }
        $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
    }
} else {
    session_destroy();
    echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
}
?>

