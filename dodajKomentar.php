<?php

include_once 'lib.php';
$baza = new Baza();
session_start();
if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
    if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {

        $id_korisnika = $_SESSION["ID"];
        $id_dogadjaja = $_POST["idDogadjaja"];
        $komentar = $_POST["komentar"];
        
        $baza->dodajKomentar($id_korisnika, $id_dogadjaja, $komentar);
        echo json_encode((object) ["Greska" => "NEMA", "Komentari" => $komentar]);

        $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
    }
} else {
    session_destroy();
    echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
}
?>