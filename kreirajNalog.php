<?php
include_once 'lib.php';
include_once 'Korisnik.php';
include_once 'posaljiMail.php';

if(isset($_POST["lozinka"])){
    $baza=new Baza();
    $email = $_POST["email"];
    $korisnickoIme=$_POST["korisnickoIme"];
    $lozinka=$_POST["lozinka"];
    if($baza->postojiKorisnik($email)==false){
        $hash=password_hash($lozinka, PASSWORD_DEFAULT);
        $korisnik=new Korisnik(-1,$korisnickoIme,$email,$hash,'NE');
        if($baza->kreirajNalog($korisnik)==true)
        {
         
            session_start();
            $_SESSION['PRIJAVLJEN'] = true;
            $_SESSION['POSLEDNJA_AKTIVNOST']=time();
            $_SESSION['ADMIN']='NE';
            $_SESSION['EMAIL']=$korisnik->email;
            $_SESSION["ID"]=$baza->vratiIdKorisnika($email);
            echo json_encode((object)["Prijavljen"=>true]);
            posaljiMail($korisnik->email,"ThOr","Uspesno ste se prijavili");
         
        }
        else{
            echo json_encode((object)["Greska"=>"Greska u bazi"]);
        }
        
    }
    else
    {
       echo json_encode((object)["Greska"=>"email"]);
    }

}
 ?>