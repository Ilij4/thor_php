<?php

include_once 'Dogadjaj.php';
include_once 'Sala.php';
include_once 'Korisnik.php';
include_once 'Info.php';

class Baza {

    const db_host = "remotemysql.com";
    const db_korisnicko_ime = "WmiHTdTZWt";
    const db_lozinka = "jnm5uhmxfc";
    const db_ime_baze = "WmiHTdTZWt";

    function vratiDogadjaje($datum) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select * from dogadjaj where aktivan=1 AND datum_pocetka>='$datum'");
            if ($res) {
                $dogadjaji = array();
                while ($row = $res->fetch_assoc()) {

                    $dogadjaj = new Dogadjaj($row["id"], $row["naziv"], $row["datum_pocetka"], $row["cena"], $row["duzina_trajanja"], $row["aktivan"], $row["opis"], $row["url_slike"], $row["idSale"], $row["prosecnaOcena"], $row["vreme_odrzavanja"]);

                    $dogadjaji[] = $dogadjaj;
                }
                $res->close();
                return $dogadjaji;
            } else {
                print ("Query failed");
            }
        }
    }

    function dodajDogadjaj(Dogadjaj $dogadjaj) {

        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("INSERT INTO dogadjaj (naziv, datum_pocetka, cena, duzina_trajanja,aktivan,opis,url_slike,idSale,prosecnaOcena,vreme_odrzavanja)
        VALUES('$dogadjaj->naziv', '$dogadjaj->datum_pocetka' , $dogadjaj->cena , '$dogadjaj->duzina_trajanja' ,'1','$dogadjaj->opis','$dogadjaj->url_slike','$dogadjaj->idIzabraneSale','$dogadjaj->prosecnaOcena','$dogadjaj->vreme_odrzavanja');");
            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function dodajUTabeluZauzetaMesta($idDogadjaj, $zauzetaMesta) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("INSERT INTO ZauzetaMesta (idDogadjaj,zauzetaMesta)
        VALUES('$idDogadjaj', '$zauzetaMesta') ;");
            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function vratiSale() {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select * from sala");
            if ($res) {
                $sale = array();
                //__construct($id,$naz,$datum,$cena,$duzina,$aktivan,$opis,$url)
                while ($row = $res->fetch_assoc()) {
                    $sala = new Sala($row["id"], $row["naziv"], $row["konfiguracija_mesta"], $row["default_zauzeta_sedista"]);
                    $sale[] = $sala;
                }
                $res->close();
                return $sale;
            } else {
                print ("Query failed");
            }
        }
    }

    function dodajSalu(Sala $sala) {
        //INSERT INTO `sala`(`id`, `naziv`, `konfiguracija_mesta`) VALUES ([value-1],[value-2],[value-3])

        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("INSERT INTO sala (naziv, konfiguracija_mesta,default_zauzeta_sedista)
        VALUES('$sala->naziv', '$sala->konfiguracija_mesta', '$sala->default_zauzeta_sedista' ) ;");
            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function kreirajNalog(Korisnik $korisnik) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("INSERT INTO korisnik (username, email,hash)
        VALUES('$korisnik->username', '$korisnik->email','$korisnik->hash') ;");
            if (!$res) {
                echo(mysqli_error($con));
                return false;
            }
            return true;
        }
    }

    function vratiKorisnika($email) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select id,username,email,hash,admin,primaj_obavestenja from korisnik where email='$email'");
            if ($res) {
                if (mysqli_num_rows($res) == 0) {
                    $res->close();
                    return null;
                }

                $row = $res->fetch_assoc();
                $ret = new Korisnik($row["id"], $row["username"], $row["email"], $row["hash"], $row['admin']);
                $ret->primajObavestenja = $row["primaj_obavestenja"];
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function vratiKorisnikovUsername($idKorisnika) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select username from korisnik where id='$idKorisnika'");
            if ($res) {
                if (mysqli_num_rows($res) == 0) {
                    $res->close();
                    return null;
                }
                $row = $res->fetch_assoc();
                $ret = $row["username"];
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function postojiKorisnik($email) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select username from korisnik where email='$email'");
            if ($res) {
                $ret = true;
                if (mysqli_num_rows($res) == 0)
                    $ret = false;
                $res->close();
                return $ret;
            }
            else {
                print ("Query failed");
            }
        }
    }

    function vratiIdPoslednjegDogadjaja() {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select max(id) from dogadjaj ");
            if ($res) {
                $row = $res->fetch_assoc();
                $ret = $row["max(id)"];
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function vratiNKorisnikaPoId($od, $Kolko) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select * from korisnik  ORDER BY ID ASC LIMIT $od, $Kolko");
            if ($res) {
                $ret = array();
                while ($row = $res->fetch_assoc()) {
                    $ret[] = new Korisnik($row["id"], $row["username"], $row["email"], null, $row['admin']);
                }
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function vratiBrojKorisnika() {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select count(1) FROM korisnik");
            if ($res) {
                $row = $res->fetch_array();

                $total = $row[0];
                $res->close();
                return $total;
            } else {
                print ("Query failed");
                return 0;
            }
        }
    }

    function vrati_zauzeta_mesta($idDogadjaja) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select zauzetaMesta from ZauzetaMesta where idDogadjaj='$idDogadjaja'");
            if ($res) {

                //__construct($id,$naz,$datum,$cena,$duzina,$aktivan,$opis,$url)

                $row = $res->fetch_assoc();

                $zauzeta_mesta = $row["zauzetaMesta"];

                $res->close();
                return $zauzeta_mesta;
            } else {
                print ("Query failed");
            }
        }
    }

    function azurirajZakazanaMesta($id, $azurirani_string) {

        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("UPDATE ZauzetaMesta SET zauzetaMesta='$azurirani_string' WHERE idDogadjaj='$id'");
            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function azurirajInfo($naziv, $grad, $ulica, $kontakt, $radnoVreme, $lokacijaX, $lokacijaY) {

        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("UPDATE pozoriste SET naziv='$naziv', ulica='$ulica', grad='$grad', kontakt='$kontakt', radnoVreme='$radnoVreme', lokacijaX='$lokacijaX', lokacijaY='$lokacijaY'  WHERE id=1");
            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function vratiIdKorisnika($email) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select id from korisnik where email='$email'");
            if ($res) {
                if (mysqli_num_rows($res) == 0) {
                    $res->close();
                    return null;
                }
                $row = $res->fetch_assoc();
                $ret = $row['id'];
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function dodajRezervaciju($id_korisnika, $id_dogadjaja, $string_mesta) {

        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("INSERT INTO Rezervacija(idKorisnika, idDogadjaja, mesta) VALUES ('$id_korisnika','$id_dogadjaja','$string_mesta')");
            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function vratiDogadjajeZaKorisnika($od, $kolko, $id) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select dogadjaj.* from Rezervacija,dogadjaj WHERE Rezervacija.idDogadjaja=dogadjaj.id AND Rezervacija.idKorisnika=$id group by dogadjaj.id ORDER BY dogadjaj.id ASC LIMIT $od, $kolko");
            if ($res) {
                $ret = array();
                while ($row = $res->fetch_assoc()) {
                    $ret[] = new Dogadjaj($row["id"], $row["naziv"], $row["datum_pocetka"], $row["cena"], $row["duzina_trajanja"], $row["aktivan"], $row["opis"], $row["url_slike"], $row["idSale"], $row["prosecnaOcena"], $row['vreme_odrzavanja']);
                }
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function vratiBrojDogadjajaZaKorisnika($idKorisnika) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select count(DISTINCT dogadjaj.id) from Rezervacija,dogadjaj WHERE Rezervacija.idDogadjaja=dogadjaj.id AND Rezervacija.idKorisnika=$idKorisnika");
            if ($res) {
                $row = $res->fetch_array();

                $total = $row[0];
                $res->close();
                return $total;
            } else {
                print ("Query failed");
                return 0;
            }
        }
    }

    function vratiKorisnikeZaObavestenje() {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select email from korisnik where primaj_obavestenja='DA'");
            if ($res) {
                $ret = array();
                while ($row = $res->fetch_assoc()) {
                    $ret[] = $row["email"];
                }
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function dodajKomentar($idKorisnika, $id_dogadjaja, $komentar) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("INSERT INTO Komentari(idDogadjaja, idKorisnik, komentar,datum) VALUES ('$id_dogadjaja','$idKorisnika','$komentar',CURDATE())");

            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function vratiInformacijeOkomentarima($idDogadjaja) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select * from Komentari where idDogadjaja='$idDogadjaja'");
            if ($res) {
                $ret = array();
                while ($row = $res->fetch_assoc()) {
                    $info = (object) ['id' => $row["id"], 'idKorisnik' => $row["idKorisnik"], 'komentar' => $row["komentar"], 'datum' => $row["datum"], 'username' => " "];
                    $ret[] = $info;
                }
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function vratiInfo() {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select * from pozoriste");
            $info;
            if ($res) {

                while ($row = $res->fetch_assoc()) {
                    $info = new Info($row["id"], $row["naziv"], $row["grad"], $row["ulica"], $row["kontakt"], $row["radnoVreme"], $row["lokacijaY"], $row["lokacijaX"]);
                }
                $res->close();
                return $info;
            } else {
                print ("Query failed");
            }
        }
    }

    function promeniKorisnikovePodatke($korisnik) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("UPDATE korisnik set username='$korisnik->username',email='$korisnik->email',hash='$korisnik->hash', primaj_obavestenja='$korisnik->primajObavestenja' where id=$korisnik->id");
            if (!$res) {
                echo(mysqli_error($con));
                return false;
            }
            return true;
        }
    }

    function vratiNDogadjaja($od, $Kolko, $filter) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {

            $res = $con->query("select * from dogadjaj $filter ORDER BY datum_pocetka DESC LIMIT $od, $Kolko");
            if ($res) {
                $ret = array();
                while ($row = $res->fetch_assoc()) {
                    $ret[] = new Dogadjaj($row["id"], $row["naziv"], $row["datum_pocetka"], $row["cena"], $row["duzina_trajanja"], $row["aktivan"], $row["opis"], $row["url_slike"], $row["idSale"], $row["prosecnaOcena"], $row["vreme_odrzavanja"]);
                }
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function vratiBrojDogadjaja($filter) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {
            $res = $con->query("select count(ID) from dogadjaj $filter");
            if ($res) {
                $row = $res->fetch_array();

                $total = $row[0];
                $res->close();
                return $total;
            } else {
                print ("Query failed");
                return 0;
            }
        }
    }

    function vratiOcenu($idDogadjaja) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {

            $res = $con->query("select prosecnaOcena from dogadjaj where id='$idDogadjaja'");
            if ($res) {
                $row = $res->fetch_assoc();
                $ocena = $row["prosecnaOcena"];

                $res->close();
                return $ocena;
            } else {

                print ("Query failed");
            }
        }
    }

    function dodajOcenu($idDogadjaja, $idKorisnika, $ocena) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("INSERT INTO OceneDogadjaja(idDogadjaj, idKorisnik, ocena) VALUES ('$idDogadjaja','$idKorisnika','$ocena')");

            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function azurirajOcenjivanje($idDogadjaja, $idKorisnika, $ocena) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("UPDATE OceneDogadjaja SET ocena='$ocena' WHERE idDogadjaj='$idDogadjaja' AND idKorisnik='$idKorisnika'");

            if (!$res) {
                echo(mysqli_error($con));
            }
        }
    }

    function vecOcenjivaoDogadjaj($idKorisnika, $idDogadjaja) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("select * from OceneDogadjaja where idDogadjaj='$idDogadjaja' AND idKorisnik='$idKorisnika'");
            if ($res) {
                $ret = true;
                if (mysqli_num_rows($res) == 0)
                    $ret = false;
                $res->close();
                return $ret;
            }
            else {
                echo(mysqli_error($con));
            }
        }
    }

    function izracunajNovuOcenu($idDogadjaja) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("select AVG(ocena) from OceneDogadjaja where idDogadjaj='$idDogadjaja'");
            if ($res) {
                $row = $res->fetch_assoc();
                $ret = $row["AVG(ocena)"];

                $res->close();
                return $ret;
            } else {
                echo(mysqli_error($con));
            }
        }
    }

    function azurirajOcenuDogadjaja($idDogadjaja, $nova_ocena) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {

            $res = $con->query("UPDATE dogadjaj SET prosecnaOcena='$nova_ocena' WHERE id='$idDogadjaja'");

            if (!$res) {
                return (mysqli_error($con));
            }
        }
    }

    function izbrisiKomentar($idKomentara) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {

            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("DELETE FROM Komentari WHERE id='$idKomentara'");
            if ($res) {

                $res->close();
                return true;
            } else {
                echo(mysqli_error($con));
            }
        }
    }

    function suspendujKorisnika($idKorisnika, $od, $do) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {
            print ("Greška pri povezivanju sa bazom podataka ($con->connect_errno): $con->connect_error");
        } else {
            $res = $con->query("insert into suspenzija (id_korisnika,datum_od,datum_do) VALUES($idKorisnika,'$od','$do')");
            if ($res) {

                return true;
            } else {
                echo(mysqli_error($con));
                return false;
            }
        }
    }

    function korisnikJeSuspendovan($idKorisnika, $datum) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {

            $res = $con->query("select datum_do from suspenzija where id_korisnika=$idKorisnika AND datum_od<='$datum' AND datum_do>'$datum'");
            if ($res) {
                $ret = true;
                if (mysqli_num_rows($res) == 0)
                    $ret = false;
                else {
                    $ret = $res->fetch_assoc()["datum_do"];
                }
                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function suspendovanDo($idKorisnika) {
        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset("utf8");
        if ($con->connect_errno) {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        } else {

            //$res = $con->query("select MIN(datum_od),MAX(datum_do) from suspenzija where id_korisnika=$idKorisnika");
            $res = $con->query("select id,datum_od,datum_do from suspenzija where id_korisnika=$idKorisnika");

            if ($res) {
                $ret = array();
                while ($row = $res->fetch_assoc()) {
                    $pom = (object) ["Id" => $row["id"], "Od" => $row["datum_od"], "Do" => $row["datum_do"]];
                    $ret[] = $pom;
                }

                $res->close();
                return $ret;
            } else {
                print ("Query failed");
            }
        }
    }

    function podesiAktivnost($idDogadjaja, $aktivnost) {

        $con = new mysqli(self::db_host, self::db_korisnicko_ime, self::db_lozinka, self::db_ime_baze);
        $con->set_charset('utf8');
        if ($con->connect_errno) {
            return false;
        } else {

            $res = $con->query("UPDATE dogadjaj SET aktivan=$aktivnost WHERE id='$idDogadjaja'");
            if (!$res) {
                print (mysqli_error($con));
                return false;
            } else {

                return true;
            }
        }
    }

}

?>
