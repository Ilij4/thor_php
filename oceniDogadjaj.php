<?php

include_once 'lib.php';
$baza = new Baza();
session_start();
if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
    if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {

        $idKorisnika = $_SESSION['ID'];

        $vec_ocenjivao = $baza->vecOcenjivaoDogadjaj($idKorisnika, $_POST["idDogadjaja"]);
        if ($vec_ocenjivao == false) {
            $baza->dodajOcenu($_POST["idDogadjaja"], $idKorisnika, $_POST["ocena"]);
        } else {
            $baza->azurirajOcenjivanje($_POST["idDogadjaja"], $idKorisnika, $_POST["ocena"]);
        }

        $nova_ocena = $baza->izracunajNovuOcenu($_POST["idDogadjaja"]);

        $gres = $baza->azurirajOcenuDogadjaja($_POST["idDogadjaja"], $nova_ocena);


        echo json_encode((object) ["Greska" => $gres, "NovaOcena" => $nova_ocena]);


        $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
    }
} else {
    session_destroy();
    echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
}
?>