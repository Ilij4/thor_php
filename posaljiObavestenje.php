<?php
include_once 'Korisnik.php';
include_once 'posaljiMail.php';
if(isset($_POST["naslov"])){
    session_start();
    if(isset($_SESSION['PRIJAVLJEN'])&&$_SESSION['PRIJAVLJEN']==true){
        if(isset($_SESSION['POSLEDNJA_AKTIVNOST'])&&time()-$_SESSION['POSLEDNJA_AKTIVNOST']<3600)
        {
            if(isset($_SESSION['ADMIN'])&&$_SESSION['ADMIN']=='DA')
            {   
                $naslov=$_POST["naslov"];
                $sadrzaj=$_POST["sadrzaj"];
                posaljiMejlSvima($naslov, $sadrzaj);
                
                echo json_encode((object)["Greska"=>"NEMA"]);
            }
            else {
                echo json_encode((object)["Greska"=>"NISTE_ADMIN"]);
            }
            $_SESSION['POSLEDNJA_AKTIVNOST']=time();
        }
        else{
            session_destroy();
            echo json_encode((object)["Greska"=>"SESIJA_ISTEKLA"]);
            
        }
    }
    else{
        session_destroy();
        echo json_encode((object)["Greska"=>"NIJE_PRIJAVLJEN"]);
    }
  }
?>

