<?php

include_once 'lib.php';
include_once 'Korisnik.php';


if (isset($_POST["lozinka"])) {
    $email = $_POST["email"];
    $lozinka = $_POST["lozinka"];
    $baza = new Baza();
    $korisnik = $baza->vratiKorisnika($email);

    if ($korisnik != null) {
        if (password_verify($lozinka, $korisnik->hash)) {
            $do = $baza->korisnikJeSuspendovan($korisnik->id, date("Y-m-d"));
            if ($do == false) {
                session_start();
                $_SESSION['PRIJAVLJEN'] = true;
                $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
                $_SESSION['ADMIN'] = $korisnik->admin;
                $_SESSION['EMAIL'] = $korisnik->email;
                $_SESSION["ID"] = $korisnik->id;
                echo json_encode((object) ['Greska' => 'Nema', "Prijava" => 'Uspesna', 'Admin' => $korisnik->admin]);
            } else {
                echo json_encode((object) ['Greska' => "Suspenzija", "Do" => $do]);
            }
        } else {
            echo json_encode((object) ['Greska' => "lozinka"]);
        }
    } else {
        echo json_encode((object) ['Greska' => "email"]);
    }
}