<?php

    session_start();
    if(isset($_SESSION['PRIJAVLJEN'])&&$_SESSION['PRIJAVLJEN']==true){
        if(isset($_SESSION['POSLEDNJA_AKTIVNOST'])&&time()-$_SESSION['POSLEDNJA_AKTIVNOST']<3600)
        {
            IF(isset($_SESSION['ADMIN'])&&$_SESSION['ADMIN']=='DA')
            {   
                echo json_encode((object)["Guest"=>false,"Admin"=>true]);
            }
            else
                echo json_encode((object)["Guest"=>false,"Admin"=>false]);
        }
        else{
            session_destroy();
            echo json_encode((object)["Guest"=>true,"Admin"=>false]);
            
        }
    }
    else{
        session_destroy();
        echo json_encode((object)["Guest"=>true,"Admin"=>false]);
    }