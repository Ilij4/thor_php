<?php

include_once 'lib.php';
include_once 'Korisnik.php';
session_start();

$baza = new Baza();
if (isset($_POST["datumOd"])) {
    if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
        if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {
            IF (isset($_SESSION['ADMIN']) && $_SESSION['ADMIN'] == 'DA') {
                $idKorisnika = $_POST["idKorisnika"];
                $od = $_POST["datumOd"];
                $do = $_POST["datumDo"];
                if ($baza->suspendujKorisnika($idKorisnika, $od, $do)) {
                    echo json_encode((object) ["Greska" => "Nema"]);
                } else {
                    echo json_encode((object) ["Greska" => "Nastala prilikom unosa u bazu"]);
                }
            } else {
                echo json_encode((object) ["Greska" => "NISTE_ADMIN"]);
            }
            $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
        } else {
            session_destroy();
            echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
        }
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
    }
}
?>

