<?php

include_once 'lib.php';
include_once 'posaljiMail.php';

$baza = new Baza();
session_start();
if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
    if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {

        if (isset($_SESSION['EMAIL'])) {
            $email = $_SESSION['EMAIL'];
            $id_korisnika = $baza->vratiIdKorisnika($email);
            $id_dogadjaja = $_POST["idDogadjaja"];
            $string_mesta = $_POST["string_mesta"];
            $naslov = $_POST["naslov"];
            $datum=$_POST["datum"];
            $vreme=$_POST["vreme"];
            $baza->dodajRezervaciju($id_korisnika, $id_dogadjaja, $string_mesta);

            echo json_encode((object) ["Greska" => "NEMA"]);
            posaljiMail($_SESSION["EMAIL"], "Rezervacija: " . $naslov, "Dogadjaj: $naslov <br> Odrzava se: $datum , $vreme <br> Rezervisali ste sledeca mesta: " . $string_mesta);
            $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
        } else {
            session_destroy();
            echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
        }
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
    }
}
?>