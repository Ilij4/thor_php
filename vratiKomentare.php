<?php
include_once 'lib.php';
include_once 'Dogadjaj.php';

$baza=new Baza();
if(isset($_POST["idDogadjaja"])){
    session_start();
    if(isset($_SESSION['PRIJAVLJEN'])&&$_SESSION['PRIJAVLJEN']==true){
        if(isset($_SESSION['POSLEDNJA_AKTIVNOST'])&&time()-$_SESSION['POSLEDNJA_AKTIVNOST']<3600)
        {
           $idDogadjaja = $_POST["idDogadjaja"];
            $informacije = $baza->vratiInformacijeOkomentarima($idDogadjaja);
            foreach ($informacije as $x) {
                $x->username = $baza->vratiKorisnikovUsername($x->idKorisnik);
            }
            echo json_encode((object)["Greska"=>"NEMA","Informacije"=>$informacije]);
            $_SESSION['POSLEDNJA_AKTIVNOST']=time();
        }
        else{
            session_destroy();
            echo json_encode((object)["Greska"=>"SESIJA_ISTEKLA"]);
            
        }
    }
    else{
        session_destroy();
        echo json_encode((object)["Greska"=>"NIJE_PRIJAVLJEN"]);
    }
  }
?>

