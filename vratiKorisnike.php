<?php
include_once 'lib.php';
include_once 'Korisnik.php';

$baza=new Baza();
if(isset($_POST["stranica"])){
    session_start();
    if(isset($_SESSION['PRIJAVLJEN'])&&$_SESSION['PRIJAVLJEN']==true){
        if(isset($_SESSION['POSLEDNJA_AKTIVNOST'])&&time()-$_SESSION['POSLEDNJA_AKTIVNOST']<3600)
        {
            IF(isset($_SESSION['ADMIN'])&&$_SESSION['ADMIN']=='DA')
            {   
                $stranica=$_POST["stranica"];
                $brojKorisnika=$_POST["brojKorisnika"];
                $korisnici=$baza->vratiNKorisnikaPoId(($stranica-1)*$brojKorisnika,$brojKorisnika);
                echo json_encode((object)["Greska"=>"NEMA","Korisnici"=>$korisnici]);
            }
            else {
                echo json_encode((object)["Greska"=>"NISTE_ADMIN"]);
            }
            $_SESSION['POSLEDNJA_AKTIVNOST']=time();
        }
        else{
            session_destroy();
            echo json_encode((object)["Greska"=>"SESIJA_ISTEKLA"]);
            
        }
    }
    else{
        session_destroy();
        echo json_encode((object)["Greska"=>"NIJE_PRIJAVLJEN"]);
    }
  }
?>

