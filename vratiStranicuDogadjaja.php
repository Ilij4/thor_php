<?php

include_once './lib.php';
include_once './Korisnik.php';

$baza = new Baza();
if (isset($_POST["stranica"])) {
    session_start();
    if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
        if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {
            IF (isset($_SESSION['ADMIN']) && $_SESSION['ADMIN'] == 'DA') {

                $filter = "";
                if (isset($_POST["minCena"])) {
                    $filter = "WHERE cena>={$_POST["minCena"]} AND cena<={$_POST["maxCena"]} AND datum_pocetka >='{$_POST['DatumOd']}' AND datum_pocetka <= '{$_POST['DatumDo']}'";
                }

                $stranica = $_POST["stranica"];
                $brojDogadjaja = $_POST["brojDogadjaja"];
                $idKorisnika = $_SESSION["ID"];
                $dogadjaji = $baza->vratiNDogadjaja(($stranica - 1) * $brojDogadjaja, $brojDogadjaja, $filter);

                $ukupno = 0;
                if (isset($_POST["vratiMax"])) {
                    $ukupno = $baza->vratiBrojDogadjaja($filter);
                }

                echo json_encode((object) ["Greska" => "NEMA", "Dogadjaji" => $dogadjaji, "Ukupno" => $ukupno, "Filter" => $filter]);
            } else {
                echo json_encode((object) ["Greska" => "NISTE_ADMIN"]);
            }
            $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
        } else {
            session_destroy();
            echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
        }
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
    }
}
?>

