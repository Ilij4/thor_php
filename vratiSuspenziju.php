<?php
include_once 'lib.php';
include_once 'Korisnik.php';
session_start();

$baza=new Baza();
if(isset($_POST["idKorisnika"])){
    if(isset($_SESSION['PRIJAVLJEN'])&&$_SESSION['PRIJAVLJEN']==true){
        if(isset($_SESSION['POSLEDNJA_AKTIVNOST'])&&time()-$_SESSION['POSLEDNJA_AKTIVNOST']<3600)
        {
            IF(isset($_SESSION['ADMIN'])&&$_SESSION['ADMIN']=='DA')
            {   
                $idKorisnika=$_POST["idKorisnika"];
                $odg=$baza->suspendovanDo($idKorisnika);
                echo json_encode((object)["Greska"=>"Nema","Datum"=>$odg]);
            }
            else {
                echo json_encode((object)["Greska"=>"NISTE_ADMIN"]);
            }
            $_SESSION['POSLEDNJA_AKTIVNOST']=time();
        }
        else{
            session_destroy();
            echo json_encode((object)["Greska"=>"SESIJA_ISTEKLA"]);
            
        }
    }
    else{
        session_destroy();
        echo json_encode((object)["Greska"=>"NIJE_PRIJAVLJEN"]);
    }
  }
?>

