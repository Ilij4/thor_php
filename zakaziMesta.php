<?php

include_once 'lib.php';
$baza = new Baza();
session_start();
if (isset($_SESSION['PRIJAVLJEN']) && $_SESSION['PRIJAVLJEN'] == true) {
    if (isset($_SESSION['POSLEDNJA_AKTIVNOST']) && time() - $_SESSION['POSLEDNJA_AKTIVNOST'] < 3600) {

        $novi_string_indeksi = $_POST["string_zauzetih_indeksa"];
        $id = $_POST["idDogadjaja"];
        $novi_array_indeksi = explode(",", $novi_string_indeksi);
        $stari_string = $baza->vrati_zauzeta_mesta($id);
        $stari_array = str_split($stari_string);
        foreach ($novi_array_indeksi as $x) {
            $stari_array[$x] = 1;
        }
        $azurirani_string = implode("", $stari_array);
        $baza->azurirajZakazanaMesta($id, $azurirani_string);
        echo json_encode((object)["Greska"=>"NEMA","Ret"=>$azurirani_string]);
        $_SESSION['POSLEDNJA_AKTIVNOST'] = time();
    } else {
        session_destroy();
        echo json_encode((object) ["Greska" => "SESIJA_ISTEKLA"]);
    }
} else {
    session_destroy();
    echo json_encode((object) ["Greska" => "NIJE_PRIJAVLJEN"]);
}
?>